#!/usr/bin/env groovy

pipeline {
    agent any
    environment{
        def mvnExec = "/var/maven_home/apache-maven-3.5.4/bin/mvn"
    }
    parameters {
       choice(choices: 'database-service\ncas-service\nStormProject', description: 'artifact ID of the RPM to build', name: 'PRODUCT_NAME')
       string(defaultValue: 'development', description: 'branch to build', name: 'BRANCH_NAME')
       string(defaultValue: '', description: 'If blank, the product build number will increment.\n example, "1.2.3.4" will become "1.2.3.5"', name: 'NEW_PRODUCT_VERSION')
        // choices are newline separated
    }
    stages{
        stage('Test'){
            steps {
                sh '''
                if [ -d dir1 ]; then
                   rm -rf dir1
                fi
                if [ -d dir2 ]; then
                   rm -rf dir2
                fi
                if [ -d dir3 ]; then
                   rm -rf dir3
                fi
                mkdir dir1
                cd dir1
                git clone https://gitlab.com/thetezz/${PRODUCT_NAME}.git
                cd ${PRODUCT_NAME}
                git checkout ${BRANCH_NAME}
                ${mvnExec} package
                '''
            }
        }
        
        stage('Version'){
            steps {
                sh '''
                cd dir1/${PRODUCT_NAME}
                if [ "${NEW_PRODUCT_VERSION}" == "" ]; then
                   ${mvnExec} com.t3zz:pipeline-util-maven-plugin:incrementVersionProperty -DpropertyName=releasedVersion -Dmode=4
                else
                   ${mvnExec} com.t3zz:pipeline-util-maven-plugin:setVersionProperty -DpropertyName=releasedVersion -DpropertyValue=${NEW_PRODUCT_VERSION}
                fi
                ${mvnExec} com.t3zz:pipeline-util-maven-plugin:nodeToEnv -Dxpath=/project/properties/releasedVersion
                source ./RELEASEDVERSION_env.sh
                if [ $(git tag -l "${RELEASEDVERSION}") ]; then
                   echo "tag: ${RELEASEDVERSION} already exists."
                   exit 1
                fi
                ${mvnExec} versions:set -DnewVersion=${RELEASEDVERSION}
                git add pom.xml
                git commit -m "Updated Version"
                git push origin ${BRANCH_NAME}
                '''
            }
        }


        stage('Merge'){
            steps {
                sh '''
                mkdir dir2
                cd dir2
                git clone https://gitlab.com/thetezz/${PRODUCT_NAME}.git
                cd ${PRODUCT_NAME}
                git merge origin/${BRANCH_NAME}
                git push origin master
                '''
            }
        }
        stage('Package'){ //or deploy????
            steps {
                sh '''
                cd dir2/${PRODUCT_NAME}
                ${mvnExec} package
                ${mvnExec} com.t3zz:pipeline-util-maven-plugin:nodeToEnv -Dxpath=/project/properties/releasedVersion
                source ./RELEASEDVERSION_env.sh
                git tag -a ${RELEASEDVERSION} -m "added this tag"
                git push origin --tags 
                '''
            }
        }
        stage('Update'){
            steps {
                sh '''
                cd dir2/${PRODUCT_NAME}
                rm RELEASEDVERSION_env.sh
                ${mvnExec} com.t3zz:pipeline-util-maven-plugin:incrementVersionProperty -DpropertyName=releasedVersion -Dmode=4
                ${mvnExec} com.t3zz:pipeline-util-maven-plugin:nodeToEnv -Dxpath=/project/properties/releasedVersion
                source ./RELEASEDVERSION_env.sh
                cd ../..
                mkdir dir3
                cd dir3
                git clone https://gitlab.com/thetezz/${PRODUCT_NAME}.git
                cd ${PRODUCT_NAME}
                git checkout ${BRANCH_NAME}
                ${mvnExec} versions:set -DnewVersion=${RELEASEDVERSION}-SNAPSHOT
                git add pom.xml
                git commit -m "Increment and added SNAPSHOT qualifier to version"
                git push origin ${BRANCH_NAME}
                '''
//sync release here!
            }
        }
    }
}
